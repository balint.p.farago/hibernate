package com.matritel.kedua.entity.repository;

import com.matritel.kedua.entity.Person;

import java.util.List;

public interface PersonRepository {

    Person getByID(int id);

    void save(Person person);

    void update(Person person);

    void delete(Person person);

    List<Person> getAllPersonsWithHQL();

    List<Person> getAllPersons();

    List<Person> getAllPersonsDeprecated();


}
