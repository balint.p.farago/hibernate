package com.matritel.kedua.entity.repository;

// This class is used to provide a sessionfactory which will give us a session

import com.matritel.kedua.entity.Person;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory;  // sessionFactory provide the session (the connection with the database)
    // It must be static to always get the same sessionFactory when we would like to use it

    public HibernateUtil() {
    }

    public static void createSessionFactory(){
        if (sessionFactory != null){
            return;
        }

        Configuration configuration = new Configuration();

        //here we tell the hibernate what entity we would like to connect to the database?
        //configuration.addAnnotatedClass(Person.class); // the method needs a Class type parameter that's why I have to add '.class'

        //Using xml mapping it must be used the addClass instead of addAnnotatedClass
        configuration.addClass(Person.class);
        sessionFactory = configuration.buildSessionFactory();
    }

    public static SessionFactory getSessionFactory(){
        if (sessionFactory == null){
            createSessionFactory();
        }
        return sessionFactory;
    }
}

