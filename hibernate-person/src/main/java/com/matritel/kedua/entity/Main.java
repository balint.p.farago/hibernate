package com.matritel.kedua.entity;

import com.matritel.kedua.entity.repository.HibernateUtil;
import com.matritel.kedua.entity.repository.PersonRepositoryImplementation;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {

        PersonRepositoryImplementation pRI = new PersonRepositoryImplementation();
        /*Person p1 = pRI.getByID(1);
        System.out.println(p1);
        pRI.save(new Person("Zoltam", "Ujj", "1973-01-01", "Zoli"));
        pRI.update(new Person());*/

        System.out.println(pRI.getAllPersonsWithHQL());


        Person p1 = pRI.getByID(27);
        p1.setNickName("Zoli");
        pRI.update(p1);

        System.out.println(pRI.getAllPersonsWithHQL());
        pRI.closeSession();
        //if we close all the sessions after we need to close the sessionfactory
        HibernateUtil.getSessionFactory().close();
    }
}
