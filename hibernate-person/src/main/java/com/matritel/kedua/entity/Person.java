package com.matritel.kedua.entity;

import javax.persistence.*;

//we need to annotate the class itself first
// we tells the hibernate that the class is an entity
//@Entity
// we tell hibernate which table we are looking for and after that
// we tell hibernate we have a unique constraint "ID" and tell the name of the table
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = "ID"), name = "person")
//QUESTION: why we need to define just id as unique key? we also have unique key: nickname

public class Person {

    /*@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)   //QUESTION:  Why is it necessary? What does it mean?
    @Column(name = "ID")*/
    private Integer ID;


   // @Column(name = "FIRSTNAME")
    private String firstName;

    //@Column(name = "LASTNAME")
    private String lastName;

    //@Column(name = "BIRTHDAY")
    private String birthday;

    //@Column(name = "NICKNAME")
    private String nickName;

    public Person() {
    }

    public Person(String firstName, String lastName, String birthday, String nickName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.nickName = nickName;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "ID=" + ID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", nickName='" + nickName + '\'' +
                '}' + "\n";
    }
}
